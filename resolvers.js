const { Post } = require('./models');

async function getHomePosts(args) {
	const skip = args.page == 0 ? 0 : +args.page * +args.i + +args.i;
	const limit = +args.page == 0 ? +args.i * 2 : +args.i;
	try {
		const posts = await Post.find().sort({ $natural: -1 }).skip(skip).limit(limit);
		return posts;
	} catch (err) {
		return err.message;
	}
}

async function getPagePosts(args) {
	try {
		const post = await Post.find({ slug: args.slug });
		const previous = await Post.find({ _id: { $gt: post[0]._id } }).limit(1);
		const next = await Post.find({ _id: { $lt: post[0]._id } })
			.sort({ _id: -1 })
			.limit(1);
		const postObj = {
			post: post[0],
			next: next[0],
			previous: previous[0],
		};
		return postObj;
	} catch (err) {
		return err.message;
	}
}

module.exports = { getHomePosts, getPagePosts };
