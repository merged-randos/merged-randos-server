const mongoose = require('mongoose');
const { Schema } = mongoose;

const postSchema = new Schema({
	image: String,
	title: String,
	slug: String,
	caption: String,
	sources: [],
	timestamp: { type: Date, default: Date.now },
});

const Post = mongoose.model('post', postSchema);

module.exports = {
	Post: Post,
};
