const fs = require('fs');
const gm = require('gm');

const cropImage = (objs) => {
	const editor = objs.map((obj) => {
		return new Promise((resolve, reject) => {
			gm(obj.path)
				.resize(obj.width, obj.height)
				.crop(800, 800, obj.x, obj.y)
				.write(obj.path, function (err) {
					if (err) {
						reject(err);
					}
					resolve(obj);
				});
		});
	});
	return Promise.all(editor);
};

module.exports = cropImage;
