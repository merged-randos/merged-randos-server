const gm = require('gm');

// do the calulations needed to crop the image, and return an array of objects including that data
const calculateImageCrop = (objs) => {
	const sizer = objs.map((obj, index) => {
		return new Promise((resolve, reject) => {
			gm(obj.path).size(function (err, size) {
				if (err) {
					reject(err);
				} else if (!size?.width || !size?.height) {
					reject('no size');
				} else {
					// find the shorter side of image and divide our desired finsihed dimension (800) by it to get our multiplier
					// so we can resize the image to only need to be cropped on one axis
					// ie make the shorter side the finished length we want, then crop the longer side
					const mult = size.width < size.height ? 800 / size.width : 800 / size.height;

					// find the offest for cropping the longer side, so that we're left with the middle of the picture
					// apply the multiplier to get the image to its pre cropping size
					// subtract our desired finsihed dimension (800) to get the remainder, then divide the remainder in half to get the offset
					const axis = size.width < size.height ? { x: 0, y: (size.height * mult - 800) / 2 } : { x: (size.width * mult - 800) / 2, y: 0 };

					resolve({
						width: size.width * mult,
						height: size.height * mult,
						...axis,
						...obj,
					});
				}
			});
		});
	});
	return Promise.all(sizer);
};

module.exports = calculateImageCrop;
