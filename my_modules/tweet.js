var Twit = require('twit');
var T = new Twit({
	consumer_key: process.env.TWITTER_CONSUMER_KEY,
	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	access_token: process.env.TWITTER_ACCESS_TOKEN,
	access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

const tweet = (obj) => {
	return new Promise((resolve, reject) => {
		// upload our image and return an id
		T.postMediaChunked({ file_path: `./assets/images/${obj.image}` }, function (err, data, response) {
			if (err) {
				reject(err);
			}
			const mediaId = data.media_id_string;

			// image alt text for accessibility's sake
			const meta_params = {
				media_id: mediaId,
				alt_text: { text: `a double exposure photograph programmically generated from the words ${obj.title}` },
			};

			// upload the alt text
			T.post('media/metadata/create', meta_params, function (err, data, response) {
				if (err) {
					reject(err);
				}

				// the tweet consisting of text and the id of our image upload
				const caption = obj.caption.length < 240 ? obj.caption : `${obj.caption.substring(0, 240)}...`;
				const params = {
					status: `${caption} https://mergedrandos.com/${obj.slug}`,
					media_ids: [mediaId],
				};

				// post the tweet
				T.post('statuses/update', params, function (err, data, response) {
					if (err) {
						reject(err);
					}
					resolve(obj);
				});
			});
		});
	});
};

module.exports = tweet;
