const gm = require('gm');

const overlayImage = (obj) => {
	return new Promise((resolve, reject) => {
		gm(obj.sources[0].path) // open first image
			.composite(obj.sources[1].path) // overlay second image
			.dissolve(50) // give second image 50% opacity
			.write(`./assets/images/${obj.image}`, function (err) {
				if (err) {
					reject(err);
				}
				resolve(obj);
			});
	});
};

module.exports = overlayImage;
