const fs = require('fs');
const axios = require('axios');

const downloadImages = (objs) => {
	const saver = objs.map((obj, index) => {
		const path = `./assets/temp/image-${index}.jpg`;
		return new Promise((resolve, reject) => {
			axios({
				method: 'get',
				url: obj.url,
				responseType: 'stream',
			}).then((response) => {
				response.data.pipe(
					fs
						.createWriteStream(path)
						.on('error', (error) => {
							reject(error.message);
						})
						.on('finish', () => {
							resolve({ path, ...obj });
						})
				);
			});
		});
	});
	return Promise.all(saver);
};

module.exports = downloadImages;
