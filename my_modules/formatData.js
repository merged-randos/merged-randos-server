const { nanoid } = require('nanoid'); // makes unique id

const formatData = (objs) => {
	const title = `${objs[0].term.charAt(0).toUpperCase() + objs[0].term.slice(1)} ${objs[1].term.charAt(0).toUpperCase() + objs[1].term.slice(1)}`;
	const slug = `${objs[0].term}-${objs[1].term}-${nanoid()}`;
	const image = `${slug}.jpg`;
	return {
		title,
		slug,
		image,
		sources: [
			{
				site: objs[0].site,
				page: objs[0].link,
				term: objs[0].term,
				path: objs[0].path,
			},
			{
				site: objs[1].site,
				page: objs[1].link,
				term: objs[1].term,
				path: objs[1].path,
			},
		],
	};
};

module.exports = formatData;
