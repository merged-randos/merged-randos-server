const axios = require('axios');

function randoItem(array) {
	const index = Math.floor(Math.random() * array.length);
	return array[index];
}

async function fetchCC(obj) {
	const res = await axios(`https://api.creativecommons.engineering/v1/images?q=${obj.term}&page_size=100&extension=jpg`);
	const imageObj = randoItem(res.data.results);
	return {
		url: imageObj.url,
		link: imageObj.foreign_landing_url,
		...obj,
	};
}

async function fetchWiki(obj) {
	const search = await axios(`https://commons.wikimedia.org/w/api.php?action=query&list=search&srsearch=${obj.term}&srlimit=100&format=json`).then(
		(response) => response.data.query.search
	);
	const searchObj = randoItem(search);
	const pages = await axios(
		`https://commons.wikimedia.org/w/api.php?action=query&generator=images&prop=imageinfo&gimlimit=100&redirects=1&titles=${searchObj.title}&iiprop=timestamp|user|userid|comment|canonicaltitle|url|size|dimensions|sha1|mime|thumbmime|mediatype|bitdepth&format=json`
	).then((response) => response.data.query.pages);
	const keys = Object.keys(pages);
	const images = keys.map((item) => pages[item].imageinfo[0]);
	const jpegImages = images.filter((item) => item.mime == 'image/jpeg');
	const imageObj = randoItem(jpegImages);
	return {
		url: imageObj.url,
		link: imageObj.descriptionurl,
		...obj,
	};
}

module.exports = { randoItem, fetchCC, fetchWiki };
