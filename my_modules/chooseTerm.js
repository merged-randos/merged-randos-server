const terms = require('./terms.js');
const { randoItem } = require('./helpers');

const chooseTerm = () => {
	// this is a promise because its the first function in the composePost.js promise chain
	return new Promise((resolve, reject) => {
		resolve([
			{
				site: 'wiki',
				term: randoItem(terms),
			},
			{
				site: 'cc',
				term: randoItem(terms),
			},
		]);
	});
};

module.exports = chooseTerm;
