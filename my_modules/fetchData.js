const { fetchWiki, fetchCC } = require('./helpers');

function fetchData(objs) {
	const res = objs.map(async (obj) => (obj.site === 'wiki' ? await fetchWiki(obj) : await fetchCC(obj)));
	return Promise.all(res);
}

module.exports = fetchData;
