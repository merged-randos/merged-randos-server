const { Post } = require('../models');

const dbWriter = (obj) => {
	const dbPost = new Post(obj);

	return new Promise((resolve, reject) => {
		dbPost.save(function (err, doc) {
			if (err) {
				reject(err);
			}
			resolve(obj);
		});
	});
};

module.exports = dbWriter;
