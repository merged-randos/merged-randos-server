const deepai = require('deepai');
deepai.setApiKey(process.env.DEEP_AI_KEY);

const generateCaption = async (obj) => {
	var resp = await deepai.callStandardApi('text-generator', {
		text: obj.title.charAt(0) + obj.title.slice(1).toLowerCase(),
	});
	const joinedParagraphs = resp.output.replace(/[\r\n]+/gm, ' ');
	const slicedSentences = joinedParagraphs.match(/\S[^.?!]*[.?!]/g);
	const captionRaw = slicedSentences[0].length > obj.title.length + 9 ? slicedSentences[0] : `${slicedSentences[0]} ${slicedSentences[1]}`;
	const caption = captionRaw.length > 501 ? `${captionRaw.substring(0, 500)}...` : captionRaw;
	return {
		caption: caption,
		...obj,
	};
};

module.exports = generateCaption;
