const chooseTerm = require('./chooseTerm');
const fetchData = require('./fetchData');
const downloadImages = require('./downloadImages');
const calculateImageCrop = require('./calculateImageCrop');
const cropImage = require('./cropImage');
const formatData = require('./formatData');
const overlayImage = require('./overlayImage');
const generateCaption = require('./generateCaption');
const writeDb = require('./writeDb');
const tweet = require('./tweet');

function composePost() {
	chooseTerm()
		.then((result) => fetchData(result))
		.then((result) => downloadImages(result))
		.then((result) => calculateImageCrop(result))
		.then((result) => cropImage(result))
		.then((result) => formatData(result))
		.then((result) => overlayImage(result))
		.then((result) => generateCaption(result))
		.then((result) => writeDb(result))
		.then((result) => {
			if (process.env.NODE_ENV == 'production') {
				tweet(result);
			} else {
				return result;
			}
		})
		.then((result) => console.log(`Success - ${new Date(Date.now())} - ${result.title}`))
		.catch((error) => console.log(`Error - ${new Date(Date.now())} - ${error.message}`));
}

composePost();

module.exports = composePost;
