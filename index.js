require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const cron = require('node-cron');
const { getHomePosts, getPagePosts } = require('./resolvers');
const composePost = require('./my_modules/composePost');

cron.schedule('0 * * * *', function () {
	composePost();
});

const url = 'mongodb://localhost:27017/dx';
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.once('open', () => console.log(`mongo --> ${url}`));

const app = express();
const port = 4000;
app.use('/images', express.static('./assets/images'));
app.use(cors());
app.get('/posts', async (req, res) => {
	const posts = req.query.home ? await getHomePosts(req.query) : await getPagePosts(req.query);
	res.status(200).json(posts);
});
app.listen({ port: port }, () => console.log(`express --> http://localhost:${port}`));
